$(document).ready(function(){
  $('.slick').slick({
    dots: true,
    autoplay:false,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),

  })
  $('.slick-clients').slick({
    dots: false,
    autoplay:false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 2,
    prevArrow: $('.prev2'),
    nextArrow: $('.next2'),
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: false,
          centerMode: true,
       centerPadding: '20px',
        }
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
         centerPadding: '50px',
        }
      }
    ]
  })

  $(".web").click(function(){
    $('.thumbnails li').hide({duration:600});
    $(".web-thumb").show({duration:0});
  });
  $(".photo").click(function(){
    $('.thumbnails li').hide({duration:600});
    $(".photo-thumb").show({duration:0} );
  });
  $(".Identity").click(function(){
    $('.thumbnails li').hide({duration:600});
    $(".Identity-thumb").show({duration:0});
  });
  $(".all").click(function(){
    $('.thumbnails li').show({duration:1100});
  });

  $('.js-anchor-link').click(function(e){
    e.preventDefault();
    var target = $($(this).attr('href'));
    if(target.length){
      var scrollTo = target.offset().top;
      $('body, html').animate({scrollTop: scrollTo+'px'}, 1200);
    }
  });


  $(".js-anchor-link").click(function(){
    $('.js-anchor-link').removeClass('active');
    $(this).addClass('active');
  });
  $('.scrollup').click(function(){
    $('body, html').animate({scrollTop: 0}, 1200);
  });
  $(".navbar-toggle").click(function(){
    $('.navbar .nav').slideToggle(500);
  });


  $(window).scroll(function(){
    var scrollTop = $(this).scrollTop();
    if (scrollTop > $('.navbar').height()) {
      $('.navbar').addClass('slideDown');
    } else {
      $('.navbar').removeClass('slideDown');
    }

    if (scrollTop > $('.navbar').height()) {
      $('.js-anchor-link').removeClass('active')
      $(this).addClass('active')
    }

    if (scrollTop >  $('#service').height()) {
      $('.scrollup').show({duration:600});
    }    else {
      $('.scrollup').hide({duration:600});
    }

    if (scrollTop > 2066) {
      $('#about .thumbnail').addClass('pulse');
    } else  {
      $('#about .thumbnail').removeClass('pulse');
    }

    if (scrollTop > 2600) {
      $('.progres-80').addClass('bar-progres-80');
      $('.progres-95').addClass('bar-progres-95');
      $('.progres-68').addClass('bar-progres-68');
      $('.progres-70').addClass('bar-progres-70');
    }
    if (scrollTop > 708) {
      $('.fade-in').addClass('fade');
    }    else {
      $('.fade-in').removeClass('fade');
    }
    if (scrollTop > 3508) {
      $('.fade-in2').addClass('fade');
    } else {
      $('.fade-in2').removeClass('fade');
    }
  });
});
